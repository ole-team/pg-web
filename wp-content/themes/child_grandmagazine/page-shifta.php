<?php
/**
 * The main template file for display page.
 *
 * @package WordPress
*/

/**
*   Get Current page object
**/
if(!is_null($post))
{
    $page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*   Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 
?>

<?php
    //Include custom header feature
	get_template_part("/templates/template-header");
?>

    <div class="inner">
        <!-- Begin main content -->
        <div class="inner_wrapper">
            <div class="sidebar_content content full_width">
                <div class="titulo_clientes">
                    <span>CURSOS Y PÍLDORAS</span>
                </div>
                <div class="wrap-shifta-intro">
                    <div class="logo-intro box-shifta-intro">
                        <img src="https://static.playgroundweb.com/wp-content/uploads/2025/01/logo-shifta.png" alt="">
                    </div>
                    <div class="text-intro box-shifta-intro">
                    <div id="text-init-shifta">
                    <p>Déjanos tu correo y conoce más sobre la oferta académica de SHIFTA, la Escuela Online de Creadores Digitales que es 100% online, digital y global; y busca formar a profesionales en disciplinas creativas mediante un aprendizaje que combine conceptualización y desarrollo. </p>
                    </div>
                    <div class="thankyou-msg-shifta"></div>
                    <p class="formulario-registro-shifta">

                    </p>
                    </div>
                </div>
                <div class="accordion-container"></div>
<style>
#wrapper.menu_transparent .top_bar{background-color: #000 !important;}
.page .content .accordion-container .accordion .title strong{font-size: 19px;  color: #000;}
span.info-cat{font-family: 'Work Sans', sans-serif !important;
    font-weight: 600;
    background: #fdff2c !important;
    border-radius: 15px;
    color: #000;
    margin: 0px;
    padding: 8px 15px;
    display: block;
    font-size: 12px;
    letter-spacing: normal;
    line-height: 12px;margin-left: -15px;}
.wrap-shifta-intro{display: flex;justify-content: center;align-items: center;} 
.box-shifta-intro{width: 45%;padding: 20px;}  
.logo-intro{ text-align: center;align-self: flex-start;}
.logo-intro img{ width: 100%; height: auto; margin: 0 auto;}
.wrap-shifta-intro input[type=text], .wrap-shifta-intro input[type=email]{
    background: #fff;
    border: none;
    border-bottom: 1px solid #588AFB;
    margin: 10px 0px;
    border-radius: 0px;
    width: 100%;
}
.wrap-shifta-intro select {
    width: 100%;
    background-color: #fff !important;
    border: none;
    border-bottom: 1px solid #588AFB;
    margin: 10px 0px;
    border-radius: 0px;
    padding: 10px 5px;
}
.wrap-shifta-intro button {
    width: auto !important;
    padding: 5px 30px;
    text-align: center;
    font-family: 'IBM Plex Sans', sans-serif;
    font-size: 18px;
    background-color: #588AFB;
    border: none;
    border-radius: 5px;
    color: #fff; margin-top:15px;
}
@media (max-width: 768px) {
    .wrap-shifta-intro {
        flex-direction: column;
    }
    .box-shifta-intro {
        width: 100%;
        padding: 10px;
    }
}
</style>
            </div>
        </div>
        <!-- End main content -->
    </div> 
</div>
<br class="clear"/><br/>
<?php get_footer(); ?>