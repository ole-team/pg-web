<?php
/**
 * The main template file for display page.
 *
 * @package WordPress
*/

/**
*   Get Current page object
**/
if(!is_null($post))
{
    $page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*   Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 
?>

<?php
    //Include custom header feature
    get_template_part("/templates/template-header-empleo");
?>

    <div class="inner">
        <!-- Begin main content -->
        <div class="inner_wrapper">
            <div class="sidebar_content content full_width">
                <div class="titulo_clientes">
                    <span>Empleo</span>
                </div>
            <div class="div_empleos">
                <div class="caja_empleo first">
                <h2 class="titulo_empleo">Quiero
                    <span class="titulo_empleo">ser</span>
                    <span class="titulo_empleo">PlayGrounder</span>
                </h2>
                    <div class="div_imagen">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/img/playgrounder.png" alt="">
                    </div>
                </div>
            </div>
            <?php 
                if ( have_posts() ) {
                while ( have_posts() ) : the_post(); ?>     
        
                <?php the_content(); break;  ?>

            <?php endwhile; 
            }
            ?>
            <?php
            if (comments_open($post->ID)) 
            {
            ?>
            <br/><div class="fullwidth_comment_wrapper">
                <?php comments_template( '', true ); ?>
            </div>
            <?php
            }
            ?>
            </div>
        </div>
        <!-- End main content -->
    </div> 
</div>
<br class="clear"/><br/>
<?php get_footer(); ?>