<?php
 $tg_blog_display_tags = kirki_get_option('tg_blog_display_tags');

    if(has_tag() && !empty($tg_blog_display_tags))
    {
?>
	<div class="wrap-suscribe">
	<iframe src="https://playgroundmag.substack.com/embed" width="100%" height="150" style="border:1px solid #EEE; background:white;" frameborder="0" scrolling="no"></iframe>
	</div>
    <div class="post_excerpt post_tag">
    	<?php
	    	if( $tags = get_the_tags() ) {
			    foreach( $tags as $tag ) {
			    	if($tag->name!= 'nohome'){
			    		echo '<a href="' . get_term_link( $tag, $tag->taxonomy ) . '">#' . $tag->name . '</a>';
			    	}
			    }
			}	
	   	?>
    </div>
<?php
    }
?>
