<?php
$idp = get_the_ID();
$autor_old = get_post_meta( $idp, 'autor_pg', true);
if(empty($autor_old)){
	$author_id = get_post_field ('post_author', $post_id);
	$author_name = get_the_author_meta('display_name', $author_id);
	$author_bio = get_the_author_meta('description', $author_id);
	$author_link = get_author_posts_url($author_id);
	if(!empty($author_bio)){ ?>
		<div class="post_info_author_bottom"> 
		<?php
		$avatarlink = get_avatar($author_id);
		if(empty($avatarlink)){
			echo '<div class="avatar_author"><img src="https://static.playground.media/wp-content/uploads/2020/11/avatar_pg-1-150x150.jpg" /></div>';
		}else{
			echo '<div class="avatar_author">'.get_avatar($author_id, '100' ).'</div>';
		}
		echo '<div class="col_info_author"><a href="'.$author_link.'"><span>'.$author_name.'</span></a><p>'.$author_bio.'</p></div>';
		?>
			
		</div>
	<?php }
}
?>
