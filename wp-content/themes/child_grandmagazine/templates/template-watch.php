<?php 
	$category_videos = get_cat_ID('Videos');
 ?>
<div id="category" class="contenedor_videos">
	<div class="contenedor">
		<h2><?php echo esc_html_e( 'Watch This!', 'grandmagazine' ); ?></h2>
		<img src="https://static.playgroundweb.com/wp-content/uploads/2021/04/pg_emoji_watchThis.png" class="emoji_watchThis" />
			<?php 
			$the_query = new WP_Query(array('tag' => 'video','posts_per_page' => '1') );
			if ( $the_query->have_posts() ) :
			    while ( $the_query->have_posts() ) : $the_query->the_post();
			    	echo '<!--'.$post->ID.'--><div class="wrap_box_video">';
					$vidposition = get_post_meta($post->ID,'video_position',true);
					$videoid = get_post_meta($post->ID,'video_id',true);
					if($vidposition == 'top' && !empty($videoid)){
						echo '<script src="https://geo.dailymotion.com/player/xr2uk.js" data-video="'.$videoid.'"></script>';
						echo '<div class="tt_box_video"><a href="'.get_permalink().'"><h5>' . get_the_title() . '</h5></a></div>';
					}else{
						if ( has_post_thumbnail() ) { 
							$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
							echo '<a href="'.get_permalink().'"><div class="icon_play_box"><img src="'.get_stylesheet_directory_uri().'/img/boton-de-play.svg"/></div><img src="'.$image[0].'" /><div class="tt_box_video"><h5>' . get_the_title() . '</h5></div></a>';
						} 
					}	
			        echo '</div>';
			    endwhile;
			else : ?>
			<div class="div_videos">
				<div class="wrap_video">
				<?php echo do_shortcode('[pg_dmplayer type="watchthis"]'); ?>
				</div>
			</div>
			<?php  endif; 
			wp_reset_postdata();
			?>
	</div>
</div>