<?php
/**
 * The main template file for display error page.
 *
 * @package WordPress
*/

get_header(); 
?>

<!-- Begin content -->
<div id="page_content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="error_box">
				<p class="error_type"><?php esc_html_e( '404', 'grandmagazine' ); ?></p>
			</div>
			<p class="error_text"><?php esc_html_e( 'Contenido no encontrado', 'grandmagazine' ); ?></p>
    	
	    	<div class="search_form_wrapper">
	    		<div class="content">
	    	    	<?php esc_html_e( "Este contenido no ha sido encontrado", 'grandmagazine' ); ?><br/>
	    	    	<?php esc_html_e( "Quizás le gustaría ir a nuestra página de inicio o intentar buscar a continuación.", 'grandmagazine' ); ?>
	    		</div>
	    	    
	    	    <form class="searchform" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
			    	<input style="width:100%" type="text" class="field searchform-s" name="s" value="<?php the_search_query(); ?>" placeholder="<?php esc_html_e( 'Buscar', 'grandmagazine' ); ?>">
			    </form>
    		</div>
    		<br/>
	    	
    		</div>
    		
    	</div>
    	
</div>

<?php get_footer(); ?>