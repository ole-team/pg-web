	<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

get_template_part("header-internas"); 

$grandmagazine_topbar = grandmagazine_get_topbar();

/**
*	Get current page id
**/

$current_page_id = $post->ID;

/**
*	Get current page id
**/

$current_page_id = $post->ID;

//Include custom header feature
get_template_part("/templates/single_post/template-post-header-noticia");
?>

<div class="inner">

	<!-- Begin main content -->
	<div class="inner_wrapper">

		<!-- Advertising -->
		<div class="contenedor_ad_top_cintillo">
			<!-- /4923229/pg_cintillo_atf -->
			<div id='pg_cintillo' style="min-height: 90px;">
				<script>
					googletag.cmd.push(function() { googletag.display('pg_cintillo'); });
				</script>
			</div>
		</div>

		<div class="sidebar_content">
					
<?php
if (have_posts()) : while (have_posts()) : the_post();
?>
						
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<?php
	    		//Get post featured content
			    get_template_part("/templates/single_post/template-post-featured-content");
			?>
			<div class="excerpt">
				<?php
				$vidposition = get_post_meta(get_the_ID(),'video_position',true);
				$videoid = get_post_meta(get_the_ID(),'video_id',true);
				if($vidposition == 'destacado' && !empty($videoid)){
					echo do_shortcode('[pg-dailymotion id="'.$videoid.'"]');
				}
				?>
				<?php  
			    the_excerpt();
			    ?>
			</div>
			<div class="left">
			    <?php 
				    //Share social media
				    get_template_part("/templates/single_post/template-share-social-media");
				?>
			</div>
			<div class="content">			
			<?php 
				if($vidposition == 'top' && !empty($videoid)){
					echo do_shortcode('[pg-dailymotion id="'.$videoid.'"]');
				}
				if(has_excerpt()){
					the_content();
				}else{
					$tempp = apply_filters( 'the_content', get_the_content() );
					$str = preg_replace('~<p>.*?</p>~s', '', $tempp, 1);
					echo $str;
				}
				if($vidposition == 'recomendado' && !empty($videoid)){
					echo do_shortcode('[pg-dailymotion id="'.$videoid.'"]');
				}
				$vidrel = get_post_meta(get_the_ID(),'video_recomendado',true);
				if(!empty($vidrel)){
					echo do_shortcode('[pg-dailymotion id="'.$vidrel.'"]');
				}
			?>
			</div>
			<?php 
				wp_link_pages();
				
				//Get ads after content
				echo do_shortcode(grandmagazine_get_ads('pp_ads_single_after_content'));

				//Get post tags
			    get_template_part("/templates/single_post/template-post-tags");

			?>
			<div class="left">
			    <?php 
				    //Share social media
				    get_template_part("/templates/single_post/template-share-social-media");
				?>
			</div>
			<?php
			//Get author info
			    get_template_part("/templates/single_post/template-info-autor");
			?>
			<?php if(has_tag('branded','noads')){ }else{  ?>
				<div data-type="_mgwidget" data-widget-id="1605420"></div>
				<script>(function(w,q){w[q]=w[q]||[];w[q].push(["_mgc.load"])})(window,"_mgq");</script>
			<?php }  ?>
			<br class="clear"/>			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->

<!--<div class="fullwidth_comment_wrapper sidebar">
	<?php //comments_template( '', true ); ?>
</div>-->

<?php endwhile; endif; ?>
						
    	</div>

    		<div class="sidebar_wrapper">
    		
    			<div class="sidebar_top"></div>
    		
    			<div class="sidebar">
    			
    				<div class="content">

    					<?php 
						if (is_active_sidebar('single-post-sidebar')) { ?>
		    	    		<ul class="sidebar_widget">
		    	    		<?php dynamic_sidebar('single-post-sidebar'); ?>
		    	    		</ul>
		    	    	<?php } ?>
    				
    				</div>
    		
    			</div>
    			<br class="clear"/>
    	
    			<div class="sidebar_bottom"></div>
    		</div>
    	
    </div>
    <!-- End main content -->
   
</div>

<br class="clear"/>

</div>

<?php get_footer(); ?>