<?php
/**
 * The main template file for display page.
 *
 * @package WordPress
*/

/**
*   Get Current page object
**/
if(!is_null($post))
{
    $page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*   Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 
?>

<?php
    //Include custom header feature
    get_template_part("/templates/template-header-nosotros");
?>

    <div class="inner">
        <!-- Begin main content -->
        <div class="inner_wrapper">
            <div class="sidebar_content content full_width">
            <?php 
                if ( have_posts() ) {
                while ( have_posts() ) : the_post(); ?>     
        
                <?php the_content(); break;  ?>

            <?php endwhile; 
            }
            ?>
            <div class="mobile"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/mapa-oficinas-mobile.jpg" alt=""></div>
            <div class="desktop"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/mapa-oficinas-desktop.jpg" alt=""></div>
            <?php
            if (comments_open($post->ID)) 
            {
            ?>
            <br/><div class="fullwidth_comment_wrapper">
                <?php comments_template( '', true ); ?>
            </div>
            <?php
            }
            ?>
            </div>
        </div>
        <!-- End main content -->
    </div> 
</div>
<br class="clear"/><br/>
<?php get_footer(); ?>