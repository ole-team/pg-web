<?php
/**
 * The main template file.
 *
 * @package WordPress
 */

$tg_blog_home_layout = kirki_get_option('tg_blog_home_layout');

if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['layout']))
{
	$tg_blog_home_layout = $_GET['layout'];
}

if (locate_template($tg_blog_home_layout . '.php') != '')
{
	get_template_part($tg_blog_home_layout);
}
else
{
	get_template_part('blog_3cols_newspaper');
}
?>