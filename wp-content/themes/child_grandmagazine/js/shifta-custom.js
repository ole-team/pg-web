const data = [
    {
        "titulo": "Midjourney para Creativos",
        "enlace": "https://weareshifta.com/formaciones/capsula-de-midjourney/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Presentaciones Efectivas",
        "enlace": "https://weareshifta.com/formaciones/presentaciones-efectivas/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Autodesk Revit: Introducción",
        "enlace": "https://weareshifta.com/formaciones/capsula-de-revit-introduccion/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Rhino + V-Ray: Introducción",
        "enlace": "https://weareshifta.com/formaciones/rhino-vray-introduccion/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Autodesk Revit: Avanzado",
        "enlace": "https://weareshifta.com/formaciones/capsula-de-revit-avanzado/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Creación de un Portafolio Profesional",
        "enlace": "https://weareshifta.com/formaciones/crear-portfolio/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Rhino + V-Ray: Avanzado",
        "enlace": "https://weareshifta.com/formaciones/rhino-vray-avanzado/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Adobe Illustrator",
        "enlace": "https://weareshifta.com/formaciones/adobe-illustrator/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Adobe InDesign",
        "enlace": "https://weareshifta.com/formaciones/adobe-indesign/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Creación de Imágenes con Inteligencia Artificial",
        "enlace": "https://weareshifta.com/formaciones/creando-imagenes-con-inteligencia-artificial/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "AutoCAD: Introducción",
        "enlace": "https://weareshifta.com/formaciones/autocad-introduccion/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Adobe Photoshop: Introducción",
        "enlace": "https://weareshifta.com/formaciones/adobe-photoshop-introduccion/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Figma, Prototipado UX: Introducción",
        "enlace": "https://weareshifta.com/formaciones/figma-introduccion/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "Adobe After Effects: Introducción",
        "enlace": "https://weareshifta.com/formaciones/adobe-after-effects-introduccion/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Adobe After Effects: Avanzado",
        "enlace": "https://weareshifta.com/formaciones/adobe-after-effects-avanzado/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "D5 Render",
        "enlace": "https://weareshifta.com/formaciones/capsula-de-d5-render/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Adobe Photoshop: Avanzado",
        "enlace": "https://weareshifta.com/formaciones/adobe-photoshop-avanzado/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Figma, Prototipado UX: Avanzado",
        "enlace": "https://weareshifta.com/formaciones/figma-avanzado/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "AutoCAD Avanzado",
        "enlace": "https://weareshifta.com/formaciones/autocad-avanzado/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Trends & Creative Strategy",
        "enlace": "https://weareshifta.com/formaciones/curso-trends-creative-mix/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Diseño de Web con Wordpress",
        "enlace": "https://weareshifta.com/formaciones/curso-wordpress-para-disenadores/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "Creatividad Resolutiva",
        "enlace": "https://weareshifta.com/formaciones/curso-creatividad-resolutiva/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "DesignOps: Operaciones de diseño de producto",
        "enlace": "https://weareshifta.com/formaciones/curso-en-designops/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "Investigación para Diseño",
        "enlace": "https://weareshifta.com/formaciones/curso-design-research/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "Aplicaciones creativas de la Inteligencia Artificial",
        "enlace": "https://weareshifta.com/formaciones/curso-inteligencia-artificial/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Rediseño de Mobiliario",
        "enlace": "https://weareshifta.com/formaciones/curso-en-rediseno-de-mobiliario/",
        "categoria": "Arquitectura y Renderizado"
    },
    {
        "titulo": "Brand Experience",
        "enlace": "https://weareshifta.com/formaciones/curso-brand-experience/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Enterprise UX Design",
        "enlace": "https://weareshifta.com/formaciones/curso-enterprise-ux-design/",
        "categoria": "Diseño UX y Product Design"
    },
    {
        "titulo": "Gestión de Proyectos Creativos",
        "enlace": "https://weareshifta.com/formaciones/curso-de-gestion-de-proyectos-creativos/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Visual Merchandising",
        "enlace": "https://weareshifta.com/formaciones/curso-en-visual-merchandising/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Diseñar para la Diversidad",
        "enlace": "https://weareshifta.com/formaciones/curso-diseno-inclusivo/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Marketing, Contenidos y Redes Sociales",
        "enlace": "https://weareshifta.com/formaciones/curso-en-marketing-y-redes-sociales/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    },
    {
        "titulo": "Tipografía Digital",
        "enlace": "https://weareshifta.com/formaciones/curso-tipografia-digital/",
        "categoria": "Diseño Gráfico y Edición de Imagen"
    },
    {
        "titulo": "Comunicación Disruptiva",
        "enlace": "https://weareshifta.com/formaciones/curso-comunicacion-disruptiva/",
        "categoria": "Comunicación, Estrategia y Gestión Creativa"
    }
];
    document.addEventListener('DOMContentLoaded', function () {
        const form = document.createElement('form');
        form.id = 'contactForm';

        const nameLabel = document.createElement('label');
        nameLabel.textContent = 'Nombre Completo:';
        form.appendChild(nameLabel);

        const nameInput = document.createElement('input');
        nameInput.type = 'text';
        nameInput.name = 'name';
        nameInput.required = true;
        form.appendChild(nameInput);

        const emailLabel = document.createElement('label');
        emailLabel.textContent = 'Email:';
        form.appendChild(emailLabel);

        const emailInput = document.createElement('input');
        emailInput.type = 'email';
        emailInput.name = 'email';
        emailInput.required = true;
        form.appendChild(emailInput);

        const countryLabel = document.createElement('label');
        countryLabel.textContent = 'País:';
        form.appendChild(countryLabel);

        const countrySelect = document.createElement('select');
        countrySelect.name = 'country';
        countrySelect.required = true;

        const countries = [
            'Argentina', 'Bolivia', 'Brasil', 'Chile', 'Colombia', 'Costa Rica', 'Cuba', 'Ecuador', 'El Salvador', 
            'España', 'Guatemala', 'Honduras', 'México', 'Nicaragua', 'Panamá', 'Paraguay', 'Perú', 'Puerto Rico', 
            'República Dominicana', 'Uruguay', 'Venezuela', 'Estados Unidos'
        ];

        countries.forEach(country => {
            const option = document.createElement('option');
            option.value = country;
            option.textContent = country;
            countrySelect.appendChild(option);
        });

        form.appendChild(countrySelect);

        const categoryLabel = document.createElement('label');
        categoryLabel.textContent = 'Categoría:';
        form.appendChild(categoryLabel);

        const categorySelect = document.createElement('select');
        categorySelect.name = 'category';
        categorySelect.required = true;

        const categories = [...new Set(data.map(item => item.categoria))];
        categories.forEach(category => {
            const option = document.createElement('option');
            option.value = category;
            option.textContent = category;
            categorySelect.appendChild(option);
        });

        form.appendChild(categorySelect);

        const submitButton = document.createElement('button');
        submitButton.type = 'submit';
        submitButton.textContent = 'Enviar';
        form.appendChild(submitButton);

        document.querySelector('.formulario-registro-shifta').appendChild(form);

        form.addEventListener('submit', function (event) {
            event.preventDefault();
            const selectedCategory = categorySelect.value;
            const filteredData = data.filter(item => item.categoria === selectedCategory);

            const container = document.querySelector('.thankyou-msg-shifta');
            container.innerHTML = '';
            container.appendChild(form);

            const cursosInfot = filteredData.map(item => ({
                titulo: item.titulo,
                categoria: item.categoria,
                enlace: item.enlace
            }));

            const successMessage = document.createElement('p');
            successMessage.textContent = '¡Gracias! Tu registro se completó de forma exitosa. Enviamos a tu correo electrónico la oferta de Cursos y Píldoras de SHIFTA de acuerdo a la categoría de tu interés. Revisa la carpeta de Promociones o Spam en caso de no ver nuestro correo.';
            container.prepend(successMessage);

            const formData = {
                action: 'enviar_mail_cursos',
                nombre: nameInput.value,
                email: emailInput.value,
                categoria: selectedCategory,
                cursos: JSON.stringify(cursosInfot)
            };

            fetch(ajax_object.ajax_url, {
                method: 'POST',
                body: new URLSearchParams(formData),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(response => response.json())
            .then(data => {
                if (data.success) {
                    console.log('Correo enviado exitosamente');
                } else {
                    console.error('Error al enviar el correo');
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });

            const formDataS = {
                nombre: nameInput.value,
                email: emailInput.value,
                categoria: selectedCategory,
                pais: countrySelect.value
            };

            const url = 'https://script.google.com/macros/s/AKfycbxpN7yhUBpSb9gkHrHbIXlyJkg_sVbgVcJzraMap3UlykcR5DGg9ENxV_432yWs9idm/exec'; // Reemplaza con la URL de tu Apps Script

            fetch(url, {
                method: 'POST',
                mode: 'no-cors',
                body: JSON.stringify(formDataS),
                headers: {
                    'Content-Type': 'application/json'
                }
            })
            .then(() => {
                
            })
            .catch((error) => {
                console.error('Error:', error);
                
            });
            form.style.display = 'none';
            document.getElementById('text-init-shifta').style.display = 'none';
        });
    });