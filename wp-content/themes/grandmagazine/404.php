<?php
/**
 * The main template file for display error page.
 *
 * @package WordPress
*/


get_header(); 
?>

<!-- Begin content -->
<div id="page_content_wrapper">

    <div class="inner">
    
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	
    		<div class="error_box">
				<p class="error_type"><?php esc_html_e( '404', 'grandmagazine' ); ?></p>
			</div>
			<p class="error_text"><?php esc_html_e( 'Not Found!', 'grandmagazine' ); ?></p>
    	
	    	<div class="search_form_wrapper">
	    		<div class="content">
	    	    	<?php esc_html_e( "We're sorry, the page you have looked for does not exist in our content!", 'grandmagazine' ); ?><br/>
	    	    	<?php esc_html_e( "Perhaps you would like to go to our homepage or try searching below.", 'grandmagazine' ); ?>
	    		</div>
	    	    
	    	    <form class="searchform" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
			    	<input style="width:100%" type="text" class="field searchform-s" name="s" value="<?php the_search_query(); ?>" placeholder="<?php esc_html_e( 'Type to search and hit enter...', 'grandmagazine' ); ?>">
			    </form>
    		</div>
    		<br/>
	    	
	    	<h5><?php esc_html_e( 'Or try to browse our latest posts instead?', 'grandmagazine' ); ?></h5>
	    	<hr/><br/>
	    		
	    		<div class="sidebar_content full_width three_cols">
	    		<?php
				
				$query_string ="items=6&post_type=post&paged=$paged";
				query_posts($query_string);
				$key = 0;
				
				if (have_posts()) : while (have_posts()) : the_post();
					
					$animate_layer = $key+7;
					$image_thumb = '';
					$key++;
					$image_id = get_post_thumbnail_id(get_the_ID());
					
					if($key <= 3)
					{
?>

<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header mixed">
	    	
	    		<?php
		    		$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    		$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status($post->ID);
				    		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view($post->ID);
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
					  	</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
		}
		else
		{
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('thumbnail'); ?> <?php if($key%3==0) { ?>data-column="last"<?php } else if($key%3==1) { ?>data-column="first"<?php } ?>>
	<div class="post_wrapper">
	    <div class="post_content_wrapper">
		    <div class="two_third post_header <?php if(!isset($small_image_url[0]) OR empty($small_image_url[0])) { ?>nothumbnail<?php } ?>">
			    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			    <div class="post_detail post_date">
			      <span class="post_info_date">
			      	<span>
			      		<a href="<?php the_permalink(); ?>">
				      		<i class="fa fa-clock-o"></i>
				      		<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      		</a>
			      	</span>
			      	<span class="post_info_comment">
					  	<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
					</span>
			      </span>
				</div>
		    </div>
		    
		    <div class="one_third last">
			    <?php
					if(has_post_thumbnail(get_the_ID(), 'thumbnail'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					}    
				?>
				<div class="post_img thumbnail">
				 	<a href="<?php the_permalink(); ?>">
					 	<?php 
						 	if(isset($image_thumb[0]) && !empty($image_thumb[0]))
						 	{
						?>
				 		<img src="<?php echo esc_url($image_thumb[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($image_thumb[1]); ?>px;height:<?php echo esc_attr($image_thumb[2]); ?>px;"/>
				 		<?php
					 		}
					 	?>
				 		
				 		<?php
					 		//Get post trending or hot status
						  	echo grandmagazine_get_status($post->ID);
				    	?>
				     	
				    </a>
				</div>
		    </div>
	    </div>
	</div>
</div>
<?php
		}
		
		if($key%3==0) 
		{
			echo '<br class="clear"/>';
		}
?>
<?php endwhile; endif; ?>

	    		</div>
    		</div>
    		
    	</div>
    	
</div>

<?php get_footer(); ?>