<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 */
?>

<?php echo do_shortcode(grandmagazine_get_ads('pp_ads_global_before_footer')); ?>

<?php
	//Check if blank template
	$grandmagazine_is_no_header = grandmagazine_get_is_no_header();
	$grandmagazine_screen_class = grandmagazine_get_screen_class();
	
	if(!is_bool($grandmagazine_is_no_header) OR !$grandmagazine_is_no_header)
	{

	$grandmagazine_homepage_style = grandmagazine_get_homepage_style();
	
	//Get Footer Sidebar
	$tg_footer_sidebar = kirki_get_option('tg_footer_sidebar');
?>
<br class="clear"/>
<div class="footer_bar <?php if(isset($grandmagazine_homepage_style) && !empty($grandmagazine_homepage_style)) { echo esc_attr($grandmagazine_homepage_style); } ?> <?php if(!empty($grandmagazine_screen_class)) { ?>split<?php } ?> <?php if(empty($tg_footer_sidebar)) { ?>noborder<?php } ?>">
	
	<?php
		//If display photostream
		$pp_photostream = get_option('pp_photostream');
	
		if(!empty($pp_photostream))
		{
			$photos_arr = array();
		
			if($pp_photostream == 'flickr')
			{
				$pp_flickr_id = get_option('pp_flickr_id');
				$photos_arr = grandmagazine_get_flickr(array('type' => 'user', 'id' => $pp_flickr_id, 'items' => 27));
			}
			else
			{
				$pp_instagram_username = get_option('pp_instagram_username');
				$pp_instagram_access_token = get_option('pp_instagram_access_token');
				$photos_arr = grandmagazine_get_instagram($pp_instagram_username, $pp_instagram_access_token, 27);
			}
	?>
	<div id="footer_photostream" class="footer_photostream_wrapper ri-grid ri-grid-size-3">
		<ul>
			<?php
				foreach($photos_arr as $photo)
				{
			?>
				<li><a target="_blank" href="<?php echo esc_url($photo['link']); ?>"><img src="<?php echo esc_url($photo['thumb_url']); ?>" alt="" /></a></li>
			<?php
				}
			?>
		</ul>
	</div>
	<?php
		}
	?>
	
	<?php
		$has_category = false;
		$categories_arr = get_categories();
		
		if(function_exists('z_taxonomy_image_url'))
		{
		    foreach($categories_arr as $category_arr) 
		    {
		    	if(function_exists('z_taxonomy_image_url'))
		    	{
			    	$category_id = $category_arr->term_id;
		    	    $category_image = z_taxonomy_image_url($category_id);
		    	    
		    	    if(!empty($category_image))
		    		{
		    			$has_category = true;
		    		}
		    	}
		    }
		}
		$tg_footer_browse = kirki_get_option('tg_footer_browse');
		
		if(!empty($tg_footer_browse) && $has_category)
		{
	?>
	<div id="footer_browse_category" class="standard_wrapper">
		<h2><?php echo esc_html_e( 'Browse', 'grandmagazine' ); ?></h2>
		<div class="page_tagline"><?php echo esc_html_e( 'News collects all the stories you want to read', 'grandmagazine' ); ?></div>
		<ul class="browse_category_wrapper">
			<?php
				foreach($categories_arr as $category_arr) 
				{
					$category_id = $category_arr->term_id;
					$category_link = get_category_link($category_id);
					$category_name = $category_arr->name;
					$category_image = '';
					
					if(function_exists('z_taxonomy_image_url'))
					{
						$category_image = z_taxonomy_image_url($category_id);
					}
					
					if(!empty($category_image))
					{
						$image_id = grandmagazine_get_image_id($category_image);
						$obj_image = wp_get_attachment_image_src($image_id, 'thumbnail');
			?>
					<li class="one_sixth">
						<a href="<?php echo esc_url($category_link); ?>">
							<img src="<?php echo esc_url($obj_image[0]); ?>" alt="<?php echo esc_html($category_name); ?>"/>
							<div class="browse_category_name">
								<?php echo esc_html($category_name); ?>
							</div>
						</a>
					</li>
			<?php
					}
				}
			?>
		</ul>
		<br class="clear"/>
	</div>
	<?php
		}
	?>

	<?php
	    if(!empty($tg_footer_sidebar))
	    {
	    	$footer_class = '';
	    	
	    	switch($tg_footer_sidebar)
	    	{
	    		case 1:
	    			$footer_class = 'one';
	    		break;
	    		case 2:
	    			$footer_class = 'two';
	    		break;
	    		case 3:
	    			$footer_class = 'three';
	    		break;
	    		case 4:
	    			$footer_class = 'four';
	    		break;
	    		default:
	    			$footer_class = 'four';
	    		break;
	    	}
	    	
	    	$grandmagazine_homepage_style = grandmagazine_get_homepage_style();
	?>
	<div id="footer" class="<?php if(isset($grandmagazine_homepage_style) && !empty($grandmagazine_homepage_style)) { echo esc_attr($grandmagazine_homepage_style); } ?>">
	<ul class="sidebar_widget <?php echo esc_attr($footer_class); ?>">
	    <?php dynamic_sidebar('Footer Sidebar'); ?>
	</ul>
	</div>
	<br class="clear"/>
	<?php
	    }
	?>

	<div class="footer_bar_wrapper <?php if(isset($grandmagazine_homepage_style) && !empty($grandmagazine_homepage_style)) { echo esc_attr($grandmagazine_homepage_style); } ?>">
		<?php
			//Check if display social icons or footer menu
			$tg_footer_copyright_right_area = kirki_get_option('tg_footer_copyright_right_area');
			
			if($tg_footer_copyright_right_area=='social')
			{
				if($grandmagazine_homepage_style!='flow' && $grandmagazine_homepage_style!='fullscreen' && $grandmagazine_homepage_style!='carousel' && $grandmagazine_homepage_style!='flip' && $grandmagazine_homepage_style!='fullscreen_video')
				{	
					//Check if open link in new window
					$tg_footer_social_link = kirki_get_option('tg_footer_social_link');
			?>
			<div class="social_wrapper">
			    <ul>
			    	<?php
			    		$pp_facebook_url = get_option('pp_facebook_url');
			    		
			    		if(!empty($pp_facebook_url))
			    		{
			    	?>
			    	<li class="facebook"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> href="<?php echo esc_url($pp_facebook_url); ?>"><i class="fa fa-facebook"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_twitter_username = get_option('pp_twitter_username');
			    		
			    		if(!empty($pp_twitter_username))
			    		{
			    	?>
			    	<li class="twitter"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> href="http://twitter.com/<?php echo esc_attr($pp_twitter_username); ?>"><i class="fa fa-twitter"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_flickr_username = get_option('pp_flickr_username');
			    		
			    		if(!empty($pp_flickr_username))
			    		{
			    	?>
			    	<li class="flickr"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Flickr" href="http://flickr.com/people/<?php echo esc_attr($pp_flickr_username); ?>"><i class="fa fa-flickr"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_youtube_url = get_option('pp_youtube_url');
			    		
			    		if(!empty($pp_youtube_url))
			    		{
			    	?>
			    	<li class="youtube"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Youtube" href="<?php echo esc_url($pp_youtube_url); ?>"><i class="fa fa-youtube"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_vimeo_username = get_option('pp_vimeo_username');
			    		
			    		if(!empty($pp_vimeo_username))
			    		{
			    	?>
			    	<li class="vimeo"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Vimeo" href="http://vimeo.com/<?php echo esc_attr($pp_vimeo_username); ?>"><i class="fa fa-vimeo-square"></i></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_tumblr_username = get_option('pp_tumblr_username');
			    		
			    		if(!empty($pp_tumblr_username))
			    		{
			    	?>
			    	<li class="tumblr"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Tumblr" href="http://<?php echo esc_attr($pp_tumblr_username); ?>.tumblr.com"><i class="fa fa-tumblr"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_google_url = get_option('pp_google_url');
			    		
			    		if(!empty($pp_google_url))
			    		{
			    	?>
			    	<li class="google"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Google+" href="<?php echo esc_url($pp_google_url); ?>"><i class="fa fa-google-plus"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_dribbble_username = get_option('pp_dribbble_username');
			    		
			    		if(!empty($pp_dribbble_username))
			    		{
			    	?>
			    	<li class="dribbble"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Dribbble" href="http://dribbble.com/<?php echo esc_attr($pp_dribbble_username); ?>"><i class="fa fa-dribbble"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			    		$pp_linkedin_url = get_option('pp_linkedin_url');
			    		
			    		if(!empty($pp_linkedin_url))
			    		{
			    	?>
			    	<li class="linkedin"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Linkedin" href="<?php echo esc_url($pp_linkedin_url); ?>"><i class="fa fa-linkedin"></i></a></li>
			    	<?php
			    		}
			    	?>
			    	<?php
			            $pp_pinterest_username = get_option('pp_pinterest_username');
			            
			            if(!empty($pp_pinterest_username))
			            {
			        ?>
			        <li class="pinterest"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Pinterest" href="http://pinterest.com/<?php echo esc_attr($pp_pinterest_username); ?>"><i class="fa fa-pinterest"></i></a></li>
			        <?php
			            }
			        ?>
			        <?php
			        	$pp_instagram_username = get_option('pp_instagram_username');
			        	
			        	if(!empty($pp_instagram_username))
			        	{
			        ?>
			        <li class="instagram"><a <?php if(!empty($tg_footer_social_link)) { ?>target="_blank"<?php } ?> title="Instagram" href="http://instagram.com/<?php echo esc_attr($pp_instagram_username); ?>"><i class="fa fa-instagram"></i></a></li>
			        <?php
			        	}
			        ?>
			        <?php
					    $pp_behance_username = get_option('pp_behance_username');
					    
					    if(!empty($pp_behance_username))
					    {
					?>
					<li class="behance"><a <?php if(!empty($pp_topbar_social_link_blank)) { ?>target="_blank"<?php } ?> title="Behance" href="http://behance.net/<?php echo esc_attr($pp_behance_username); ?>"><i class="fa fa-behance-square"></i></a></li>
					<?php
					    }
					?>
			    </ul>
			</div>
		<?php
				}
			} //End if display social icons
			else
			{
				if ( has_nav_menu( 'footer-menu' ) ) 
			    {
				    wp_nav_menu( 
				        	array( 
				        		'menu_id'			=> 'footer_menu',
				        		'menu_class'		=> 'footer_nav',
				        		'theme_location' 	=> 'footer-menu',
				        	) 
				    ); 
				}
			}
		?>
	    <?php
	    	//Display copyright text
	        $tg_footer_copyright_text = kirki_get_option('tg_footer_copyright_text');

	        if(!empty($tg_footer_copyright_text))
	        {
	        	echo '<div id="copyright">'.wp_kses_post(htmlspecialchars_decode($tg_footer_copyright_text)).'</div>';
	        }
	    ?>
	    
	    <?php
	    	//Check if display to top button
	    	$tg_footer_copyright_totop = kirki_get_option('tg_footer_copyright_totop');
	    	
	    	if(!empty($tg_footer_copyright_totop))
	    	{
	    ?>
	    	<a id="toTop"><i class="fa fa-angle-up"></i></a>
	    <?php
	    	}
	    ?>
	    
	    <?php
	    	$tg_boxed = kirki_get_option('tg_boxed');
		    if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['boxed']) && !empty($_GET['boxed']))
		    {
		    	$tg_boxed = 1;
		    }
		    
		    //If enable boxed layout
		    if(!empty($tg_boxed))
		    {
	    ?>
	    <br class="clear"/>
	    <?php
	    	}
	    ?>
	</div>
</div>

</div>

<?php
    } //End if not blank template
?>

<div id="side_menu_wrapper" class="overlay_background">
	<a id="close_mobile_menu" href="javascript:;"><i class="fa fa-close"></i></a>
	<?php
		if(is_single())
		{
	?>
	<div id="fullscreen_share_wrapper">
		<div class="fullscreen_share_content">
	<?php
			get_template_part("/templates/template-share");
	?>
		</div>
	</div>
	<?php
		}
	?>
</div>

<?php
    //Check if theme demo then enable layout switcher
    if(GRANDMAGAZINE_THEMEDEMO)
    {	
	    if(isset($_GET['styling']) && !empty($_GET['styling']) && file_exists(get_template_directory()."/cache/".$_GET['styling'].".css"))
	    {
		    wp_enqueue_style("grandmagazine-demo-styling", get_template_directory_uri()."/cache/".$_GET['styling'].".css", false, "", "all");
	    }
	    
	    $customizer_styling_arr = array( 
			array('id'	=>	'red', 'title' => 'Red', 'code' => '#FF3B30'),
			array('id'	=>	'orange', 'title' => 'Orange', 'code' => '#FF9500'),
			array('id'	=>	'yellow', 'title' => 'Yellow', 'code' => '#FFCC00'),
			array('id'	=>	'green', 'title' => 'Green', 'code' => '#4CD964'),
			array('id'	=>	'teal_blue', 'title' => 'Teal Blue', 'code' => '#5AC8FA'),
			array('id'	=>	'blue', 'title' => 'Blue', 'code' => '#007AFF'),
			array('id'	=>	'purple', 'title' => 'Purple', 'code' => '#5856D6'),
			array('id'	=>	'pink', 'title' => 'Pink', 'code' => '#FF2D55'),
		);
?>
    <div id="option_wrapper">
    <div class="inner">
    	<div style="text-align:center">
	    	<h5>Predefined Styling</h5>
	    	<p>
	    	Here are predefined color styling that can be imported in one click and you can also customised yours.</p>
	    	<ul class="demo_list">
		    	<?php
			    	foreach($customizer_styling_arr as $customizer_styling)
					{
				?>
	    		<li>
	        		<div class="item_content_wrapper">
						<div class="item_content">
							<a href="<?php echo esc_url(home_url('/?styling='.$customizer_styling['id'])); ?>">
					    		<div class="item_thumb" style="background:<?php echo esc_attr($customizer_styling['code']); ?>"></div>
							</a>
						</div>
					</div>		   
	    		</li>
	    		<?php
		    		}
		    	?>
	    	</ul>
    	</div>
    </div>
    </div>
    <div id="option_btn">
    	<a href="javascript:;" class="demotip" title="Choose Theme Styling"><i class="fa fa-cog"></i></a>
    	<a href="http://themegoods.theme-demo.net/grandmagazinenewsblogwordpress" class="demotip" title="Try Before You Buy" target="_blank"><i class="fa fa-edit"></i></a>
    	<a href="http://themes.themegoods.com/grandmagazine/doc/" class="demotip" title="Theme Documentation" target="_blank"><i class="fa fa-book"></i></a>
    	<a href="https://themeforest.net/item/grand-magazine-news-blog-wordpress/19054317?ref=ThemeGoods&license=regular&open_purchase_for_item_id=18556524&purchasable=source&ref=ThemeGoods" title="Purchase Theme" class="demotip" target="_blank"><i class="fa fa-shopping-basket"></i></a>
    </div>
<?php
    	wp_enqueue_script("grandmagazine-jquery-cookie", get_template_directory_uri()."/js/jquery.cookie.js", false, GRANDMAGAZINE_THEMEVERSION, true);
    	wp_enqueue_script("grandmagazine-script-demo", admin_url('admin-ajax.php')."?action=grandmagazine_script_demo", false, GRANDMAGAZINE_THEMEVERSION, true);
    }
?>

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
