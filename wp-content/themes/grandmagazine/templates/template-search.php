<?php 
if(is_search())
{
?>
<div class="search_form_wrapper">
    <?php esc_html_e( "Search results for", 'grandmagazine' ); ?> <strong><?php the_search_query(); ?></strong>. <?php esc_html_e( "If you didn't find what you were looking for, try a new search.", 'grandmagazine' ); ?><br/><br/>
    
    <form class="searchform" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
    	<input type="text" class="field searchform-s" name="s" value="<?php the_search_query(); ?>" title="<?php esc_html_e( 'Type to search and hit enter...', 'grandmagazine' ); ?>">
    	<button type="submit" id="searchsubmit" class="button"><i class="fa fa-search"></i></button>
    </form>
</div>
<?php
}

$featured_posts = grandmagazine_get_featured_posts();
if(!empty($featured_posts) && is_array($featured_posts))
{
	$query_arr = array(
		'post__not_in' => $featured_posts,
		'paged' => $paged,
		'suppress_filters' => false,
	);
	
	//If view trending
	if(isset($_GET['view']) && $_GET['view'] == 'trending')
	{
		$query_arr = array(
		    'post__not_in' => $featured_posts,
		    'post_status' => 'publish',
			'paged' => $paged,
		    'order' => 'DESC',
		    'orderby' => 'post_views',
		    'post_type' => array('post'),
		    'suppress_filters' => false
		);
	}
	
	//If view hot
	if(isset($_GET['view']) && $_GET['view'] == 'hot')
	{
		$query_arr = array(
		    'post__not_in' => $featured_posts,
		    'post_status' => 'publish',
			'paged' => $paged,
		    'orderby' => array(
			    'Share+Count' => 'DESC',
				'comment_count' => 'DESC',
				'post_views' => 'DESC',
				'ID' => 'DESC',
			),
		    'post_type' => array('post'),
		    'suppress_filters' => false,
		);
	}
	
	//If view editor picks
	$tg_blog_editor_picks_cat = kirki_get_option('tg_blog_editor_picks_cat');
	
	if(isset($_GET['view']) && $_GET['view'] == $tg_blog_editor_picks_cat)
	{
		$query_arr = array(
		    'post__not_in' => $featured_posts,
		    'post_status' => 'publish',
			'paged' => $paged,
		    'cat' => $tg_blog_editor_picks_cat,
		    'post_type' => array('post'),
		    'suppress_filters' => false,
		);
	}
	
    query_posts($query_arr);
}
else if(isset($_GET['view']))
{
	//If view trending
	if(isset($_GET['view']) && $_GET['view'] == 'trending')
	{
		$query_arr = array(
			'paged' => $paged,
			'post_status' => 'publish',
		    'order' => 'DESC',
		    'orderby' => 'post_views',
		    'post_type' => array('post'),
		    'suppress_filters' => false,
		);
	}
	
	//If view hot
	if(isset($_GET['view']) && $_GET['view'] == 'hot')
	{
		$default_posts_per_page = get_option('posts_per_page');
		
		$query_arr = array(
			'paged' => $paged,
			'post_status' => 'publish',
			'orderby' => array(
				'comment_count' => 'DESC',
				'post_views' => 'DESC',
				'ID' => 'DESC',
			),
		    'post_type' => array('post'),
		    'suppress_filters' => false,
		);
	}
	
	//If view editor picks
	$tg_blog_editor_picks_cat = kirki_get_option('tg_blog_editor_picks_cat');
	
	if(isset($_GET['view']) && $_GET['view'] == $tg_blog_editor_picks_cat)
	{
		$query_arr = array(
			'paged' => $paged,
		    'cat' => $tg_blog_editor_picks_cat,
		    'post_type' => array('post'),
		    'post_status' => 'publish',
		    'suppress_filters' => false,
		);
	}

    query_posts($query_arr);
}
?>