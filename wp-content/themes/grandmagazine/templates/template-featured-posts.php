<?php
if($paged <= 1)
{
	//Check if display slider
	$featured_posts = array();
	$tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
	$count = 1;
	
	if(!empty($tg_blog_featured_cat) && !is_search() && !is_category() && !is_tag() && !is_archive())
	{
		//Get post featured category
		$args = array( 
			'orderby' => 'date',
			'order' => 'DESC',
			'suppress_filters' => 0,
			'cat' => $tg_blog_featured_cat,
			'posts_per_page' => 6,
		);
		
		// the query
		$theme_query = new WP_Query( $args );
		
		$count_all = $theme_query->post_count;
		
		if($count_all <= 0)
		{
			$args = grandmagazine_get_default_recent_posts(6);
			$theme_query = new WP_Query( $args );
			$count_all = $theme_query->post_count;
		}
		
		if ($theme_query->have_posts()) : while ($theme_query->have_posts()) : $theme_query->the_post();
		
			array_push($featured_posts, get_the_ID());
		
			//if first post
			if($count == 1)
			{
				$post_featured_image = '';			
				if(has_post_thumbnail(get_the_ID(), 'original'))
				{
				    $image_id = get_post_thumbnail_id(get_the_ID());
				    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
				    
				    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
				    {
					    $post_featured_image = $image_thumb[0];
				    }
				}
				
				//Get post featured content
				$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);
				
				if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['content']) && $_GET['content'] == 'image')
				{
					$post_ft_type = 'Image';
				}
				
				$video_url = '';
				
				if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video')
				{
					//Add jarallax video script
					wp_enqueue_script("jarallax-video", get_template_directory_uri()."/js/jarallax-video.js", false, GRANDMAGAZINE_THEMEVERSION, true);
					
					if($post_ft_type == 'Youtube Video')
					{
						$post_ft_youtube = get_post_meta(get_the_ID(), 'post_ft_youtube', true);
						$video_url = 'https://www.youtube.com/watch?v='.$post_ft_youtube;
					}
					else
					{
						$post_ft_vimeo = get_post_meta(get_the_ID(), 'post_ft_vimeo', true);
						$video_url = 'https://vimeo.com/'.$post_ft_vimeo;
					}
				}
	?>
	<div id="featured_first_post_wrapper" class="parallax" <?php if(!empty($post_featured_image)) { ?>style="background-image:url(<?php echo esc_url($post_featured_image); ?>);"<?php } ?> <?php if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video') { ?>data-jarallax-video="<?php echo esc_url($video_url); ?>"<?php } ?>>
		<div class="background_overlay"></div>
		<a class="featured_first_post_link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
		
		<div id="post-<?php the_ID(); ?>">
			<div class="post_header">
				<div class="standard_wrapper">
				    <div class="post_header_title">
				    	<?php
				    		//Get post trending or hot status
				    		echo grandmagazine_get_status($post->ID);

							//Get Post's Categories
						    $post_categories = wp_get_post_categories($post->ID);
						    
						    //Get featured post category
						    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
						    $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
						    
						    if(!empty($post_categories))
						    {
						?>
						<div class="post_info_cat">
						    <?php
						    	$i = 0;
						    	$len = count($post_categories);
						        foreach($post_categories as $c)
						        {
						        	$cat = get_category( $c );
						        	
						        	$display_featured_cat_title = false;
						        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
						        	{
							        	$display_featured_cat_title = true;
						        	}
						        	
						        	if($display_featured_cat_title)
						        	{
						    ?>
						        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
						    <?php
								    	if(GRANDMAGAZINE_THEMEDEMO)
								    	{
									    	break;
								    	}
							    	}
							    }
							?>
						</div>
						<?php
							}
						?>
						<?php
							//Get post view counter
							$post_view = grandmagazine_get_post_view($post->ID);
							
							if(!empty($post_view))
							{
						?>
						<div class="post_info_view">
							<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
						</div>
						<?php
							}
						?>
					  	<h2><?php the_title(); ?></h2>
					  	<div class="post_detail post_date">
					  		<span class="post_info_date">
					  			<span>
					  				<a href="<?php the_permalink(); ?>">
					      				<i class="fa fa-clock-o"></i>
					       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
				      				</a>
					  			</span>
					  		</span>
					  		<span class="post_info_author">
					  			<?php
							  			$author_id = get_the_author_meta('ID', $post->post_author);
							  			$author_name = get_the_author_meta('display_name', $author_id);
							  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
							  	?>
							  	<a href="<?php echo get_author_posts_url($author_id, $author_nice_name); ?>"><span class="gravatar"><?php echo get_avatar($author_id, '60' ); ?></span><?php echo esc_html($author_name); ?></a>
					  		</span>
					  		<span class="post_info_comment">
					  			<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
					  		</span>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php	
			}
			//End if first post
			else
			{
				if($count == 2)
				{
					$image_thumb = '';
								
					if(has_post_thumbnail(get_the_ID(), 'large'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'large', true);
					}
	?>
		<div id="featured_posts_wrapper">
			<div class="standard_wrapper">
				<div class="one_half">
					<div id="post-<?php the_ID(); ?>">
						<div class="post_header">

							<?php
							    if(!empty($image_thumb))
							    {
							       	$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
							       	$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
							?>
							
							   	<div class="post_img static small">
							   	    <a href="<?php the_permalink(); ?>">
							   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
							   	    	
							   	    	<?php
								   	    	//Get post trending or hot status
										   	echo grandmagazine_get_status($post->ID);
										   			
								      		//Get post featured content
									  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
									  		
									  		switch($post_ft_type)
										    {
										    	case 'Vimeo Video':
										    	case 'Youtube Video':
										?>
													<div class="post_img_type_wrapper">
											    		<i class="fa fa-video-camera"></i>
											    	</div>
										<?php
										    	break;
										    	
										    	case 'Gallery':
										?>
										    		<div class="post_img_type_wrapper">
											    		<i class="fa fa-camera"></i>
											    	</div>
										<?php
										    	break;
										    	
										    } //End switch
								      	?>
								      	
								      	<?php
											//Get post view counter
											$post_view = grandmagazine_get_post_view($post->ID);
											
											if(!empty($post_view))
											{
										?>
										<div class="post_info_view">
											<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
										</div>
										<?php
											}
										?>
							   	    </a>
							   	</div>
						   <?php
						   		}
						   	?>
						   	<br class="clear"/>

							<div class="post_header_title">
							   	<?php
							 	//Get Post's Categories
							     $post_categories = wp_get_post_categories($post->ID);
							     
							     //Get featured post category
							     $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
							     $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
							     
							     if(!empty($post_categories))
							     {
							 ?>
							 <div class="post_info_cat">
							     <?php
							     	$i = 0;
							     	$len = count($post_categories);
							         foreach($post_categories as $c)
							         {
							         	$cat = get_category( $c );
							         	
							         	$display_featured_cat_title = false;
							        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
							        	{
								        	$display_featured_cat_title = true;
							        	}
							         	
							         	if($display_featured_cat_title)
							         	{
							     ?>
							         <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
							     <?php
							 		    	if(GRANDMAGAZINE_THEMEDEMO)
							 		    	{
							 			    	break;
							 		    	}
							 	    	}
							 	    }
							 	?>
							 </div>
							 <?php
							 	}
							 ?>
							  	<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							  	<div class="post_detail post_date">
							  		<span class="post_info_date">
							  			<span>
							  				<a href="<?php the_permalink(); ?>">
							      				<i class="fa fa-clock-o"></i>
							       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
						      				</a>
							  			</span>
							  		</span>
							  		<span class="post_info_comment">
							  			<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
							  		</span>
							  	</div>
							  	
							  	<?php
							   		echo grandmagazine_get_excerpt_by_id(get_the_ID(), 30);	
							 	?>
							</div>

						</div>
					</div>
				</div>
	<?php	
				}
				else if($count == 3)
				{
	?>
				<div class="one_half last">
	<?php
				}
				
				if($count >= 3)
				{
					$last_class = '';
					
					if($count == 4 OR $count == 6)
					{
						$last_class = 'last';
					}
	?>
				<div class="one_half <?php echo esc_attr($last_class); ?>">
					<div id="post-<?php the_ID(); ?>">
						<div class="post_wrapper">
		    
						    <div class="post_content_wrapper">
						    
						    	<div class="post_header">
							    	<?php
								    	$image_thumb = '';
								
										if(has_post_thumbnail(get_the_ID(), 'large'))
										{
										    $image_id = get_post_thumbnail_id(get_the_ID());
										    $image_thumb = wp_get_attachment_image_src($image_id, 'large', true);
										}
					
									    if(!empty($image_thumb))
									    {
									       	$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
									       	$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
									?>
									
									   	<div class="post_img static small">
									   	    <a href="<?php the_permalink(); ?>">
									   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
									   	    	
									   	    	<?php
										   	    	//Get post trending or hot status
										   			echo grandmagazine_get_status($post->ID);
				    		
										      		//Get post featured content
											  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
											  		
											  		switch($post_ft_type)
												    {
												    	case 'Vimeo Video':
												    	case 'Youtube Video':
												?>
															<div class="post_img_type_wrapper">
													    		<i class="fa fa-video-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    	case 'Gallery':
												?>
												    		<div class="post_img_type_wrapper">
													    		<i class="fa fa-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    } //End switch
										      	?>
										      	
										      	<?php
													//Get post view counter
													$post_view = grandmagazine_get_post_view($post->ID);
													
													if(!empty($post_view))
													{
												?>
												<div class="post_info_view">
													<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
												</div>
												<?php
													}
												?>
									   	    </a>
									   	</div>
								   <?php
								   		}
								   	?>
								   	<br class="clear"/>
								   	
								   	<div class="post_header_title">
								   		<?php
											//Get Post's Categories
										    $post_categories = wp_get_post_categories($post->ID);
										    
										    //Get featured post category
										    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
										    $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
										    
										    if(!empty($post_categories))
										    {
										?>
										<div class="post_info_cat">
										    <?php
										    	$i = 0;
										    	$len = count($post_categories);
										        foreach($post_categories as $c)
										        {
										        	$cat = get_category( $c );
										        	
										        	$display_featured_cat_title = false;
										        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
										        	{
											        	$display_featured_cat_title = true;
										        	}
										        	
										        	if($display_featured_cat_title)
										        	{
										    ?>
										        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
										    <?php
												    	if(GRANDMAGAZINE_THEMEDEMO)
												    	{
													    	break;
												    	}
											    	}
											    }
											?>
										</div>
										<?php
											}
										?>
								      	<h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
								      	<div class="post_detail post_date">
								      		<span class="post_info_date">
								      			<span>
								      				<a href="<?php the_permalink(); ?>">
									      				<i class="fa fa-clock-o"></i>
									       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
								      				</a>
								      			</span>
								      		</span>
								      		<span class="post_info_comment">
									  			<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
									  		</span>
									  	</div>
								   </div>
								</div>
								
						    </div>
						    
						</div>
					</div>
				</div>
	<?php	
				}
				
				if(($count > 3 && $count == $count_all) OR $count == $count_all)
				{
	?>
				</div>
	<?php
				}
				
				if($count == $count_all)
				{
	?>		</div>
		</div>
	<?php
				}
			}
			
			$count++;
			
			endwhile; endif;
			
		wp_reset_postdata();
		
		grandmagazine_set_featured_posts($featured_posts);
?>
		<br class="clear"/>
<?php		
	} //End if display slider
}
?>

<?php echo do_shortcode(grandmagazine_get_ads('pp_ads_global_after_featured')); ?>