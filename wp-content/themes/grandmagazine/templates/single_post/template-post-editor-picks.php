<?php
$tg_blog_display_editor_picks = kirki_get_option('tg_blog_display_editor_picks');
$tg_blog_editor_picks_cat = kirki_get_option('tg_blog_editor_picks_cat');

if(!empty($tg_blog_display_editor_picks) && !empty($tg_blog_editor_picks_cat))
{
	$tg_blog_display_editor_posts_items = kirki_get_option('tg_blog_display_editor_posts_items');
	
	$single_related_posts = grandmagazine_get_single_related_posts();
	
	if(is_array($single_related_posts))
	{
		array_push($single_related_posts, $post->ID);
	}
	else
	{
		$single_related_posts = array($post->ID);
	}
	
	$args = array(
      	  'cat' => $tg_blog_editor_picks_cat,
      	  'post__not_in' => $single_related_posts,
      	  'showposts' => $tg_blog_display_editor_posts_items,
      	  'orderby' => 'date',
      	  'order' => 'DESC',
      	  'suppress_filters' => false,
  	);
  	
  	$my_query = new WP_Query($args);
  	
  	$key = 0;
  	if( $my_query->have_posts() ) {
 ?>
  	<h5><?php echo esc_html_e( 'Editor Picks', 'grandmagazine' ); ?></h5><hr/>
  	<div class="post_editor">
    <?php
       while ($my_query->have_posts()) : $my_query->the_post();
       		$key++;
       		$class_name = 'one_half';
       		if($key%2==0) 
       		{
	       		$class_name = 'one_half last';
	       	}
    ?>
	<div id="post-<?php the_ID(); ?>" <?php post_class($class_name); ?>>
		<div class="post_wrapper">
		    <div class="post_content_wrapper">
			    <div class="two_third post_header">
				    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
				    <div class="post_detail post_date">
				      <span class="post_info_date">
				      	<span>
				      		<a href="<?php the_permalink(); ?>">
					      		<i class="fa fa-clock-o"></i>
					      		<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
				      		</a>
				      	</span>
				      </span>
				      <span class="post_info_comment">
						  <i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
					  </span>
					</div>
			    </div>
			    
			    <div class="one_third last">
				    <?php
						if(has_post_thumbnail(get_the_ID(), 'thumbnail'))
						{
						    $image_id = get_post_thumbnail_id(get_the_ID());
						    $image_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);   
					?>
					<div class="post_img thumbnail">
					 	<a href="<?php the_permalink(); ?>">
					 		<img src="<?php echo esc_url($image_thumb[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($image_thumb[1]); ?>px;height:<?php echo esc_attr($image_thumb[2]); ?>px;"/>
					 		
					 		<?php
						 		//Get post trending or hot status
							  	echo grandmagazine_get_status($post->ID);
					    	?>
					    </a>
					</div>
					<?php
						}
					?>
			    </div>
		    </div>
		</div>
	</div>
    <?php
       	endwhile;
       	
       	wp_reset_postdata();
       	
       	grandmagazine_set_single_related_posts($single_related_posts);
     ?>
  	</div>
<?php
  	}
} //end if show related
?>
