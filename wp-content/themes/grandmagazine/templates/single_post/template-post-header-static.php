<?php
/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/

if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}

//Get page header display setting
$page_title = get_the_title();

$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);

$pp_page_bg = '';

//Get page featured image
if(has_post_thumbnail($current_page_id, 'original'))
{
    $image_id = get_post_thumbnail_id($current_page_id); 
    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
    
    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
    {
    	$pp_page_bg = $image_thumb[0];
    }
}

$grandmagazine_topbar = grandmagazine_get_topbar();
$grandmagazine_screen_class = grandmagazine_get_screen_class();
?>
<div id="page_caption" class="<?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?> <?php if(!empty($grandmagazine_topbar)) { ?>withtopbar<?php } ?> " <?php if(!empty($pp_page_bg)) { ?>style="background-image:url(<?php echo esc_url($pp_page_bg); ?>);"<?php } ?>>

	<?php if(!empty($pp_page_bg)) { ?>
		<div class="background_overlay"></div>
	<?php } ?>
	
	<div class="page_title_wrapper">
		
		<?php if(!empty($pp_page_bg)) { ?>
			<div class="standard_wrapper">
		<?php } ?>
		
	    <div class="page_title_inner">
		    <?php
			    if(!empty($pp_page_bg)) 
			    {
					//Get post trending or hot status
					echo grandmagazine_get_status($post->ID);
				}

				//Get Post's Categories
			    $post_categories = wp_get_post_categories($post->ID);
			    
			    //Get featured post category
			    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
			    
			    if(!empty($post_categories))
			    {
			?>
			<div class="post_info_cat">
			    <?php
			    	$i = 0;
			    	$len = count($post_categories);
			        foreach($post_categories as $c)
			        {
			        	$cat = get_category( $c );
			        	
			        	if($cat->term_id != $tg_blog_featured_cat)
			        	{
			    ?>
			        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
			    <?php
					    	if(GRANDMAGAZINE_THEMEDEMO)
					    	{
						    	break;
					    	}
				    	}
				    }
				?>
			</div>
			<?php
				}
			?>
			<?php
				//Get post view counter
				$post_view = grandmagazine_get_post_view($post->ID);
				
				if(!empty($post_view))
				{
			?>
			<div class="post_info_view">
				<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
			</div>
			<?php
				}
			?>
	    	<h1 <?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>class ="withtopbar"<?php } ?>><?php echo esc_html($page_title); ?></h1>
	    	<div class="post_detail post_date">
			  <span class="post_info_date">
			  	<span>
			  		<i class="fa fa-clock-o"></i>
			  		<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			  	</span>
			  </span>
			  <span class="post_info_author">
			  	<?php
			  			$author_id = get_the_author_meta('ID', $post->post_author);
			  			$author_name = get_the_author_meta('display_name', $author_id);
			  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
			  	?>
			  	<a href="<?php echo get_author_posts_url($author_id, $author_nice_name); ?>"><span class="gravatar"><?php echo get_avatar($author_id, '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			  </span>
			  <span class="post_info_comment">
			  	<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
			  </span>
			</div>
	    </div>
	    
	    <?php if(!empty($pp_page_bg)) { ?>
			</div>
		<?php } ?>
	</div>
	<br class="clear"/>
</div>

<!-- Begin content -->
<?php
	$grandmagazine_page_content_class = grandmagazine_get_page_content_class();
?>
<div id="page_content_wrapper" class="<?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?><?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>withtopbar <?php } ?><?php if(!empty($grandmagazine_page_content_class)) { echo esc_attr($grandmagazine_page_content_class); } ?>">