<?php
/**
 * The main template file for display archive page.
 *
 * @package WordPress
*/

$tg_blog_archive_layout = kirki_get_option('tg_blog_archive_layout');

if (locate_template($tg_blog_archive_layout . '.php') != '')
{
	get_template_part($tg_blog_archive_layout);
}
else
{
	get_template_part('blog_r_list');
}
?>