<?php
/**
 * The main template file for display archive page.
 *
 * @package WordPress
*/

$tg_blog_search_layout = kirki_get_option('tg_blog_search_layout');

if (locate_template($tg_blog_search_layout . '.php') != '')
{
	get_template_part($tg_blog_search_layout);
}
else
{
	get_template_part('blog_r_list');
}
?>