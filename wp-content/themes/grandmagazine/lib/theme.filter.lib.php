<?php
$is_verified_envato_purchase_code = false;

//Get verified purchase code data
$pp_verified_envato_grandmagazine = get_option("pp_verified_envato_grandmagazine");
if(!empty($pp_verified_envato_grandmagazine))
{
	$is_verified_envato_purchase_code = true;
}

//if verified envato purchase code
if($is_verified_envato_purchase_code)
{
	function grandmagazine_import_files() {
	  return array(
	    array(
	      'import_file_name'             => 'Classic',
	      'local_import_file'            => trailingslashit( get_template_directory() ) . 'cache/demos/1.xml',
	      'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'cache/demos/1.wie',
	      'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'cache/demos/customizer/settings/pink.dat',
	      'preview_url'                  => 'http://themes.themegoods.com/grandmagazine/demo/',
	    ),
	  );
	}
	add_filter( 'pt-ocdi/import_files', 'grandmagazine_import_files' );
	
	function grandmagazine_menu_page_removing() {
	    remove_submenu_page( 'themes.php', 'tg-one-click-demo-import' );
	}
	add_action( 'admin_menu', 'grandmagazine_menu_page_removing', 99 );
	
	function grandmagazine_confirmation_dialog_options ( $options ) {
		return array_merge( $options, array(
			'width'       => 300,
			'dialogClass' => 'wp-dialog',
			'resizable'   => false,
			'height'      => 'auto',
			'modal'       => true,
		) );
	}
	add_filter( 'pt-ocdi/confirmation_dialog_options', 'grandmagazine_confirmation_dialog_options', 10, 1 );
	
	function grandmagazine_before_widgets_import( $selected_import ) {
		//Add demo custom sidebar
		if ( function_exists('register_sidebar') )
		{
		    register_sidebar(array('id' => 'custom-sidebar', 'name' => 'Custom Sidebar'));
		}
	}
	add_action( 'pt-ocdi/before_widgets_import', 'grandmagazine_before_widgets_import' );
	
	function grandmagazine_after_import( $selected_import ) {
		wp_delete_post( 1, true );
		update_option('posts_per_page',9);
		set_theme_mod('tg_blog_featured_cat', 9);
		set_theme_mod('tg_blog_editor_picks_cat', 7);
	}
	add_action( 'pt-ocdi/after_import', 'grandmagazine_after_import' );
	
	function grandmagazine_plugin_page_setup( $default_settings ) {
		$default_settings['parent_slug'] = 'themes.php';
		$default_settings['page_title']  = esc_html__( 'Demo Import' , 'grandconference' );
		$default_settings['menu_title']  = esc_html__( 'Import Demo Content' , 'grandconference' );
		$default_settings['capability']  = 'import';
		$default_settings['menu_slug']   = 'tg-one-click-demo-import';
	
		return $default_settings;
	}
	add_filter( 'pt-ocdi/plugin_page_setup', 'grandmagazine_plugin_page_setup' );
	add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );
}
	
function grandmagazine_tag_cloud_filter($args = array()) {
   $args['smallest'] = 13;
   $args['largest'] = 13;
   $args['unit'] = 'px';
   return $args;
}

add_filter('widget_tag_cloud_args', 'grandmagazine_tag_cloud_filter', 90);

//Control post excerpt length
function grandmagazine_custom_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'grandmagazine_custom_excerpt_length', 200 );

/**
 * Change default fields, add placeholder and change type attributes.
 *
 * @param  array $fields
 * @return array
 */
add_filter( 'comment_form_default_fields', 'grandmagazine_comment_placeholders' );
 
function grandmagazine_comment_placeholders( $fields )
{
    $fields['author'] = str_replace('<input', '<input placeholder="'. __('Name', 'grandmagazine'). '*"',$fields['author']);
    $fields['email'] = str_replace('<input id="email" name="email" type="text"', '<input type="email" placeholder="'.__('Email', 'grandmagazine').'*"  id="email" name="email"',$fields['email']);
    $fields['url'] = str_replace('<input id="url" name="url" type="text"', '<input placeholder="'.__('Website', 'grandmagazine').'" id="url" name="url" type="url"',$fields['url']);

    return $fields;
}

//Add Placehoder in comment Form Field (Comment)
add_filter( 'comment_form_defaults', 'grandmagazine_textarea_placeholder' );
 
function grandmagazine_textarea_placeholder( $fields )
{
    $fields['comment_field'] = str_replace(
        '<textarea',
        '<textarea placeholder="'.esc_html__('Comments*', 'grandmagazine').'"',
        $fields['comment_field']
    );
    
	return $fields;
}

//Make widget support shortcode
add_filter('widget_text', 'do_shortcode');

// remove version query string from scripts and stylesheets
function grandmagazine_remove_script_styles_version( $src ){
    return remove_query_arg( 'ver', $src );
}
add_filter( 'script_loader_src', 'grandmagazine_remove_script_styles_version' );
add_filter( 'style_loader_src', 'grandmagazine_remove_script_styles_version' );

//Add class name to post navigation links
add_filter('next_posts_link_attributes', 'grandmagazine_posts_link_attributes_prev');
add_filter('previous_posts_link_attributes', 'grandmagazine_posts_link_attributes_next');

function grandmagazine_posts_link_attributes_prev() {
    return 'class="prev_button""';
}

function grandmagazine_posts_link_attributes_next() {
    return 'class="next_button""';
}

function grandmagazine_search_form( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="searchform" action="' . esc_url(home_url( '/' )) . '" >
	<div>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" />
	<button type="submit" id="searchsubmit" class="button"><i class="fa fa-search"></i></button>
	</div>
	</form>';

	return $form;
}

add_filter( 'get_search_form', 'grandmagazine_search_form' );


function grandmagazine_add_meta_tags() {
    $post = grandmagazine_get_wp_post();
    
    //Check if responsive layout is enabled
    $tg_mobile_responsive = kirki_get_option('tg_mobile_responsive');
	
	if(!empty($tg_mobile_responsive))
	{
		echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />';
	}
	
	//meta for phone number link on mobile
	echo '<meta name="format-detection" content="telephone=no">';
	
	//Prepare data for Facebook opengraph sharing
    $pp_facebook_theme_graph = get_option('pp_facebook_theme_graph');
    
    if(empty($pp_facebook_theme_graph))
    {
	    $post_content = get_post_field('post_excerpt', $post->ID);
	    
	    echo '<meta property="og:type" content="article" />';
		echo '<meta property="og:title" content="'.esc_attr(get_the_title()).'"/>';
		echo '<meta property="og:url" content="'.esc_url(get_permalink($post->ID)).'"/>';
		echo '<meta property="og:description" content="'.esc_attr(strip_tags($post_content)).'"/>';
	    
        //Prepare data for Facebook opengraph sharing
        if(has_post_thumbnail(get_the_ID(), 'grandmagazine_blog'))
		{
		    $image_id = get_post_thumbnail_id(get_the_ID());
		    $fb_thumb = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    $fb_thumb = grandmagazine_filter_default_featued_image($fb_thumb);
		}
	
		if(isset($fb_thumb[0]) && !empty($fb_thumb[0]))
		{
			echo '<meta property="og:image" content="'.esc_url($fb_thumb[0]).'"/>';
		}
    }
}
add_action( 'wp_head', 'grandmagazine_add_meta_tags' , 2 );

add_filter('redirect_canonical','custom_disable_redirect_canonical');
function custom_disable_redirect_canonical($redirect_url) {if (is_paged() && is_singular()) $redirect_url = false; return $redirect_url; }

add_action( 'customize_register', function( $wp_customize ) {
	/**
	 * The custom control class
	 */
	class Kirki_Controls_Title_Control extends WP_Customize_Control {
		public $type = 'title';
		public function render_content() { 
			echo esc_html($this->label);
		}
	}
	// Register our custom control with Kirki
	add_filter( 'kirki/control_types', function( $controls ) {
		$controls['title'] = 'Kirki_Controls_Title_Control';
		return $controls;
	} );

} );

add_action( 'admin_enqueue_scripts', 'grandmagazine_admin_pointers_header' );

function grandmagazine_admin_pointers_header() {
   if ( grandmagazine_admin_pointers_check() ) {
      add_action( 'admin_print_footer_scripts', 'grandmagazine_admin_pointers_footer' );

      wp_enqueue_script( 'wp-pointer' );
      wp_enqueue_style( 'wp-pointer' );
   }
}

function grandmagazine_admin_pointers_check() {
   $admin_pointers = grandmagazine_admin_pointers();
   foreach ( $admin_pointers as $pointer => $array ) {
      if ( $array['active'] )
         return true;
   }
}

function grandmagazine_admin_pointers_footer() {
   $admin_pointers = grandmagazine_admin_pointers();
   ?>
<script type="text/javascript">
/* <![CDATA[ */
( function($) {
   <?php
   foreach ( $admin_pointers as $pointer => $array ) {
      if ( $array['active'] ) {
         ?>
         $( '<?php echo esc_js($array['anchor_id']); ?>' ).pointer( {
            content: '<?php echo $array['content']; ?>',
            position: {
            edge: '<?php echo esc_js($array['edge']); ?>',
            align: '<?php echo esc_js($array['align']); ?>'
         },
            close: function() {
               $.post( ajaxurl, {
                  pointer: '<?php echo esc_js($pointer); ?>',
                  action: 'dismiss-wp-pointer'
               } );
            }
         } ).pointer( 'open' );
         <?php
      }
   }
   ?>
} )(jQuery);
/* ]]> */
</script>
   <?php
}

function grandmagazine_admin_pointers() {
   $dismissed = explode( ',', (string) get_user_meta( get_current_user_id(), 'dismissed_wp_pointers', true ) );
   $prefix = 'grandmagazine_admin_pointers';

   //Page help pointers
   $page_options_content = '<h3>Page Options</h3>';
   $page_options_content .= '<p>You can customise various options for this page including menu styling, page tagline etc.</p>';
   
   $page_featured_image_content = '<h3>Page Featured Image</h3>';
   $page_featured_image_content .= '<p>Upload or select featured image for this page to displays it as background header.</p>';
   
   //Post help pointers
   $post_options_content = '<h3>Post Options</h3>';
   $post_options_content .= '<p>You can customise various options for this post including its layout and featured content type.</p>';
   
   $post_featured_image_content = '<h3>Post Featured Image (*Required)</h3>';
   $post_featured_image_content .= '<p>Upload or select featured image for this post to displays it as post image on blog, archive, category, tag and search pages.</p>';
   
   //Gallery help pointers
   $gallery_images_content = '<h3>Gallery Images</h3>';
   $gallery_images_content .= '<p>Upload or select for this gallery. You can select multiple images to upload using SHIFT or CTRL keys.</p>';
   
   $gallery_options_content = '<h3>Gallery Options</h3>';
   $gallery_options_content .= '<p>You can customise gallery template for this gallery.</p>';

   $tg_pointer_arr = array(
   
   	  //Page help pointers
      $prefix . '_page_options' => array(
         'content' => $page_options_content,
         'anchor_id' => 'body.post-type-page #page_option_page_menu_transparent',
         'edge' => 'top',
         'align' => 'left',
         'active' => ( ! in_array( $prefix . '_page_options', $dismissed ) )
      ),
      
      $prefix . '_page_featured_image' => array(
         'content' => $page_featured_image_content,
         'anchor_id' => 'body.post-type-page #set-post-thumbnail',
         'edge' => 'top',
         'align' => 'left',
         'active' => ( ! in_array( $prefix . '_page_featured_image', $dismissed ) )
      ),
      
      //Post help pointers
      $prefix . '_post_options' => array(
         'content' => $post_options_content,
         'anchor_id' => 'body.post-type-post #post_option_post_layout',
         'edge' => 'top',
         'align' => 'left',
         'active' => ( ! in_array( $prefix . '_post_options', $dismissed ) )
      ),
      
      $prefix . '_post_featured_image' => array(
         'content' => $post_featured_image_content,
         'anchor_id' => 'body.post-type-post #set-post-thumbnail',
         'edge' => 'top',
         'align' => 'left',
         'active' => ( ! in_array( $prefix . '_post_featured_image', $dismissed ) )
      ),
      
      //Gallery help pointers
      $prefix . '_gallery_options' => array(
         'content' => $gallery_options_content,
         'anchor_id' => 'body.post-type-galleries #metabox .inside',
         'edge' => 'top',
         'align' => 'left',
         'active' => ( ! in_array( $prefix . '_gallery_options', $dismissed ) )
      ),
   );

   return $tg_pointer_arr;
}

function grandmagazine_theme_admin_save_success() 
{
	if(isset($_GET['page']) && $_GET['page'] == 'functions.php' && isset($_GET['saved']) == 'true')
	{
?>
    <div class="updated notice">
        <p><?php esc_html_e( 'Successfully Update', 'grandmagazine' ); ?></p>
    </div>
<?php
	}
}
add_action( 'admin_notices', 'grandmagazine_theme_admin_save_success' );
?>