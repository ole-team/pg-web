<?php
require_once get_template_directory() . "/modules/class-tgm-plugin-activation.php";
add_action( 'tgmpa_register', 'grandmagazine_require_plugins' );
 
function grandmagazine_require_plugins() {
 
    $plugins = array(
	    array(
	        'name'               => 'Grand Magazine Theme Post Options & Gallery',
	        'slug'               => 'grandmagazine-custom-post',
	        'source'             => get_template_directory() . '/lib/plugins/grandmagazine-custom-post.zip',
	        'required'           => true, 
	        'version'            => '1.0',
	    ),
	    array(
	        'name'               	=> 'One Click Demo Import',
	        'slug'      		   	=> 'one-click-demo-import',
	        'required'        		=> true, 
	    ),
	    array(
	        'name'      => 'Post Views Counter',
	        'slug'      => 'post-views-counter',
	        'required'  => true, 
	    ),
	    array(
	        'name'      => 'Categories Images',
	        'slug'      => 'categories-images',
	        'required'  => true, 
	    ),
	    array(
	        'name'      => 'Contact Form 7',
	        'slug'      => 'contact-form-7',
	        'required'  => true, 
	    ),
	    array(
	        'name'      => 'Social Media Share Buttons | MashShare',
	        'slug'      => 'mashsharer',
	        'required'  => true, 
	    ),
	    array(
	        'name'      => 'MailChimp for WordPress',
	        'slug'      => 'mailchimp-for-wp',
	        'required'  => false, 
	    ),
	    array(
	        'name'      => 'Facebook Widget',
	        'slug'      => 'facebook-pagelike-widget',
	        'required'  => false, 
	    ),
	    array(
	        'name'      => 'WP Review',
	        'slug'      => 'wp-review',
	        'required'  => false, 
	    ),
	    array(
	        'name'      => 'WooCommerce',
	        'slug'      => 'woocommerce',
	        'required'  => true, 
	    ),
	);
	
	//If theme demo site add other plugins
	if(GRANDMAGAZINE_THEMEDEMO)
	{
		$plugins[] = array(
			'name'      => 'Disable Comments',
	        'slug'      => 'disable-comments',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Customizer Export/Import',
	        'slug'      => 'customizer-export-import',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Display All Image Sizes',
	        'slug'      => 'display-all-image-sizes',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Easy Theme and Plugin Upgrades',
	        'slug'      => 'easy-theme-and-plugin-upgrades',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Widget Importer & Exporter',
	        'slug'      => 'grandmagazine',
	        'required'  => false, 
		);
		
		$plugins[] = array(
	        'name'      => 'EWWW Image Optimizer',
	        'slug'      => 'ewww-image-optimizer',
	        'required'  => false, 
	    );
		
		$plugins[] = array(
	        'name'      => 'Imsanity',
	        'slug'      => 'imsanity',
	        'required'  => false, 
	    );
		
		$plugins[] = array(
			'name'      => 'WP Super Cache',
	        'slug'      => 'wp-super-cache',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Go Live Update URLs',
	        'slug'      => 'go-live-update-urls',
	        'required'  => false, 
		);
		
		$plugins[] = array(
			'name'      => 'Widget Clone',
	        'slug'      => 'widget-clone',
	        'required'  => false, 
		);
	}
	
	$config = array(
		'domain'	=> 'grandmagazine',
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'install-required-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'is_automatic' => true,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'          => array(
	        'page_title'                      => esc_html__('Install Required Plugins', 'grandmagazine' ),
	        'menu_title'                      => esc_html__('Install Plugins', 'grandmagazine' ),
	        'installing'                      => esc_html__('Installing Plugin: %s', 'grandmagazine' ),
	        'oops'                            => esc_html__('Something went wrong with the plugin API.', 'grandmagazine' ),
	        'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'grandmagazine' ),
	        'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'grandmagazine' ),
	        'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'grandmagazine' ),
	        'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'grandmagazine' ),
	        'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'grandmagazine' ),
	        'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'grandmagazine' ),
	        'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'grandmagazine' ),
	        'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'grandmagazine' ),
	        'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'grandmagazine' ),
	        'activate_link'                   => _n_noop( 'Activate installed plugin', 'Activate installed plugins', 'grandmagazine' ),
	        'return'                          => esc_html__('Return to Required Plugins Installer', 'grandmagazine' ),
	        'plugin_activated'                => esc_html__('Plugin activated successfully.', 'grandmagazine' ),
	        'complete'                        => esc_html__('All plugins installed and activated successfully. %s', 'grandmagazine' ),
	        'nag_type'                        => 'update-nag'
	    )
    );
 
    tgmpa( $plugins, $config );
 
}
?>